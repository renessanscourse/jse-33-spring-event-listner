package ru.ovechkin.tm.listener.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class DataBinarySaveListener extends AbstractListener {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BIN_SAVE;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[DATA BINARY SAVE]");
        storageEndpoint.dataBinarySave(sessionDTO);
        System.out.println("[OK]");
    }

}