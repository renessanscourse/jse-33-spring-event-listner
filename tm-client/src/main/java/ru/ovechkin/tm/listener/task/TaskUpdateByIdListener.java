package ru.ovechkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.TaskDTO;
import ru.ovechkin.tm.endpoint.TaskEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.TASK_UPDATE_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = taskEndpoint.findTaskById(sessionDTO, id);
        System.out.print("ENTER NEW TASK NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW TASK DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskDTO taskUpdated = taskEndpoint.updateTaskById(sessionDTO, id, name, description);
        System.out.println("[COMPLETE]");
    }

}