package ru.ovechkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

@Component
public final class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_VERSION;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_VERSION;
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info";
    }

    @Override
    @EventListener(condition = "@versionListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("1.9.0");
    }

}
