package ru.ovechkin.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.SessionEndpoint;

@Component
public class LogoutListener extends AbstractListener {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.LOGOUT;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account";
    }

    @Override
    @EventListener(condition = "@logoutListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        sessionEndpoint.signOutByUserId(sessionDTO.getUserId());
        sessionDTO.setId("");
        sessionDTO.setUserId("");
        sessionDTO.setSignature("");
        sessionDTO.setTimestamp(0L);
        System.out.println("[OK]");
    }

}