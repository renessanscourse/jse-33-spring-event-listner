package ru.ovechkin.tm.bootstrap;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public class Bootstrap {

    @Autowired
    private ConsoleEvent event;

    @Autowired
    private ApplicationEventPublisher publisher;

    public void run(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        process();
    }

    private void process() {
        String cmd = "";
        while (!CmdConst.CMD_EXIT.equals(cmd)) {
            System.out.print("Enter command: ");
            cmd = TerminalUtil.nextLine();
            try {
                event.setCommand(cmd);
                publisher.publishEvent(event);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
            System.out.println();
        }
    }

}