package ru.ovechkin.tm.exeption.empty;

public class FirstNameEmptyException extends RuntimeException {

    public FirstNameEmptyException() {
        super("Error! First name is empty...");
    }

}