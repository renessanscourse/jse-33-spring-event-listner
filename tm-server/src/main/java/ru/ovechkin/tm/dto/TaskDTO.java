package ru.ovechkin.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;

import java.io.Serializable;
import java.util.Date;

public final class TaskDTO extends AbstractDTO implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    @NotNull
    private String projectId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(@Nullable Date finishDate) {
        this.finishDate = finishDate;
    }

    @NotNull
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@NotNull String projectId) {
        this.projectId = projectId;
    }

    public TaskDTO() {
    }

    public TaskDTO(@NotNull final Task task) {
        this.name = task.getName();
        this.description = task.getDescription();
        this.userId = task.getUser().getId();
        this.startDate = task.getStartDate();
        this.finishDate = task.getFinishDate();
        this.projectId = task.getProject().getId();
        setId(task.getId());
    }

}