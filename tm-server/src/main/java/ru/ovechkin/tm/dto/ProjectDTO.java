package ru.ovechkin.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Project;

import java.io.Serializable;
import java.util.Date;

public final class ProjectDTO extends AbstractDTO implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(@Nullable Date finishDate) {
        this.finishDate = finishDate;
    }

    public ProjectDTO() {
    }

    public ProjectDTO(@NotNull final Project project) {
        this.name = project.getName();
        this.description = project.getDescription();
        this.userId = project.getUser().getId();
        this.startDate = project.getStartDate();
        this.finishDate = project.getFinishDate();
        setId(project.getId());
    }

}