package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.NameAlreadyTakenException;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.unknown.ProjectUnknownException;
import ru.ovechkin.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    public ProjectService() {
    }

    @Override
    public void add(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (projectDTO == null) return;
        @NotNull final UserDTO userDTO = context.getBean(SessionService.class).getUser(sessionDTO);
        @NotNull final User user = new User(userDTO);
        @NotNull final Project project = new Project(projectDTO);
        project.setUser(user);
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            projectRepository.add(project);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(@Nullable final SessionDTO sessionDTO, @Nullable final String name) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        add(sessionDTO, projectDTO);
    }

    @Override
    public void create(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        projectDTO.setName(name);
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        projectDTO.setDescription(description);
        add(sessionDTO, projectDTO);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findUserProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            @Nullable final List<Project> projects = projectRepository.findUserProjects(userId);
            if (projects == null || projects.isEmpty()) throw new ProjectListEmptyException();
            @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
            for (@NotNull final Project project : projects) {
                @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
                projectsDTO.add(projectDTO);
            }
            return projectsDTO;
        } catch (Exception e) {
            entityManager.close();
            throw e;
        }
    }

    @Override
    public void removeAllUserProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            projectRepository.removeAll(userId);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            @Nullable final Project project = projectRepository.findById(userId, id);
            if (project == null) throw new ProjectUnknownException();
            @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
            return projectDTO;
        } catch (Exception e) {
            entityManager.close();
            throw e;
        }
    }

    @Nullable
    @Override
    public ProjectDTO findProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            @Nullable final Project project = projectRepository.findByName(userId, name);
            if (project == null) throw new ProjectUnknownException(name);
            @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
            return projectDTO;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        if (projectRepository.findByName(userId, name) != null) {
            throw new NameAlreadyTakenException(name);
        }
        @Nullable final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new ProjectUnknownException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(project);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
        return projectDTO;
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            @Nullable final Project project = projectRepository.removeById(userId, id);
            transaction.commit();
            if (project == null) throw new ProjectUnknownException();
            @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
            return projectDTO;
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            @Nullable final Project project = projectRepository.removeByName(userId, name);
            transaction.commit();
            if (project == null) throw new ProjectUnknownException();
            @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
            return projectDTO;
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> getAllProjectsDTO() {
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            @Nullable final List<Project> projectList = projectRepository.getAllProjects();
            if (projectList == null || projectList.isEmpty()) throw new ProjectUnknownException();
            @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
            for (@NotNull final Project project : projectList) {
                @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
                projectsDTO.add(projectDTO);
            }
            return projectsDTO;
        } catch (Exception e) {
            entityManager.close();
            throw e;
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> loadProjects(@Nullable final List<ProjectDTO> projectsDTO) {
        if (projectsDTO == null || projectsDTO.isEmpty()) throw new ProjectUnknownException();
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        for (@NotNull final ProjectDTO projectDTO : projectsDTO) {
            @NotNull final Project project = new Project(projectDTO);
            projectList.add(project);
        }
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            projectRepository.mergeCollection(projectList);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return projectsDTO;
    }

    @Override
    public void removeAllProjects() {
        @NotNull final EntityManager entityManager = context.getBean(EntityManager.class);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            projectRepository.removeAllProjects();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}