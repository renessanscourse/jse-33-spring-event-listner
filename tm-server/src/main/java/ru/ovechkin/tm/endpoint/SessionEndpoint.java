package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ovechkin.tm.api.endpoint.ISessionEndpoint;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.dto.*;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Autowired
    private ISessionService sessionService;

    public SessionEndpoint() {
    }

    @NotNull
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) throws AccessForbiddenException {
        return sessionService.open(login, password);
    }

    @NotNull
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        sessionService.validate(sessionDTO);
        try {
            sessionService.close(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<SessionDTO> sessionsOfUser(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        sessionService.validate(sessionDTO);
        return sessionService.getListSession(sessionDTO);
    }

    @Override
    public boolean isValid(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO);
        return sessionService.isValid(sessionDTO);
    }

    @Override
    public void signOutByLogin(@Nullable @WebParam(name = "login", partName = "login") String login) {
        sessionService.signOutByLogin(login);
    }

    @Override
    public void signOutByUserId(@Nullable @WebParam(name = "userId", partName = "userId") String userId) {
        sessionService.signOutByUserId(userId);
    }

    @Override
    public @NotNull UserDTO getUser(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) throws AccessForbiddenException {
        sessionService.validate(sessionDTO);
        return sessionService.getUser(sessionDTO);
    }

    @Override
    public void closeAll(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) throws AccessForbiddenException {
        sessionService.validate(sessionDTO);
        sessionService.closeAll(sessionDTO);
    }

}