package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ovechkin.tm.enumerated.Role;

public abstract class AbstractEndpoint {

    @Autowired
    protected AnnotationConfigApplicationContext context;

    @Nullable
    public Role[] roles() {
        return null;
    }

}