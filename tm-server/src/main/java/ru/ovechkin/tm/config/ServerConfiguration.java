package ru.ovechkin.tm.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import ru.ovechkin.tm.service.EntityManagerService;

import javax.persistence.EntityManager;

@Configuration
@ComponentScan("ru.ovechkin.tm")
public class ServerConfiguration {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public EntityManager entityManager(EntityManagerService entityManagerService) {
        entityManagerService.init();
        return entityManagerService.getEntityManager();
    }

}