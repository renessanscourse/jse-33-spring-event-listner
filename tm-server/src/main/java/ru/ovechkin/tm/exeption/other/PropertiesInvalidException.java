package ru.ovechkin.tm.exeption.other;

public class PropertiesInvalidException extends RuntimeException {

    public PropertiesInvalidException() {
        super("Error! Invalid properties file...");
    }

}