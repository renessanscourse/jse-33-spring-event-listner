package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.TaskDTO;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(@NotNull Task task);

    void removeAll(@NotNull String userId);

    @Nullable
    List<Task> findUserTasks(@NotNull String userId);

    Task findById(@NotNull String userId, @NotNull String id);

    Task findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeById(@NotNull String userId, @NotNull String id);

    Task removeByName(@NotNull String userId, @NotNull String name);

    @Nullable List<Task> getAllTasks();

    @NotNull List<Task> mergeCollection(@NotNull List<Task> taskList);

    void removeAllTasks();
}