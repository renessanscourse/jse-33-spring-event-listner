package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void save(@Nullable Domain domain);

}